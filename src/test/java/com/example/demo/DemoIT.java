package com.example.demo;

import com.example.demo.mock.ArticleMockServer;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DemoIT extends IntegrationTest{
    @Autowired
    ArticleMockServer mockServer;

    @Override
    protected void clearDb() {
        return;
    }

    @Before
    public void setUp() {
        mockServer.setWireMockRule(wireMockRule);
    }

    @Test
    public void getTNCShouldSuccess() {
        mockServer.getArticles();

        given().when()
                .get("/v1/demo")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("meta.code", equalTo("200"))
                .body("data.data[0].id", IsEqual.equalTo("f9fd4726-2fa3-46a3-a571-83269020b26b"));
    }
}
