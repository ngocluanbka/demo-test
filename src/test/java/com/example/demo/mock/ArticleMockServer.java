package com.example.demo.mock;

import org.springframework.stereotype.Service;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Service
public class ArticleMockServer extends AbstractMockServer {
    public void getArticles() {
        wireMockRule.stubFor(
                get("/transaction-platform/v1/articles/criteria")
                        .willReturn(
                                aResponse().withBodyFile("response.json")
                        )
        );
    }
}
