package com.example.demo.mock;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;

public abstract class AbstractMockServer {

    @Rule
    WireMockRule wireMockRule;

    public void setWireMockRule(WireMockRule wireMockRule) {
        this.wireMockRule = wireMockRule;
    }

}