package com.example.demo;

import com.example.demo.response.BaseResponse;
import com.example.demo.response.Btvn2Response;
import com.example.demo.response.DataResponse;
import com.example.demo.response.UserInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/v1/demo")
public class DemoController {
    @Autowired
    ObjectMapper mapper;

    @Autowired
    @Value("${url}")
    String url;

    @Autowired
    @Value("${url2}")
    String url2;


    @GetMapping("")
    public BaseResponse<DataResponse> demo() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();

        String strResponse = restTemplate.getForObject(url, String.class);
        DataResponse response = mapper.readValue(strResponse, DataResponse.class);

        return BaseResponse.ofSucceeded(response);
    }

    @GetMapping("/btvn")
    public BaseResponse<List<UserInfo>> btvn1() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();

        String strResponse = restTemplate.getForObject(url2, String.class);
        Btvn2Response response = mapper.readValue(strResponse, Btvn2Response.class);
        return response;
    }
}
