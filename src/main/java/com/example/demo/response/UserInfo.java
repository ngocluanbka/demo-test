package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfo {
    @JsonProperty("id")
    String id;
    @JsonProperty("first_name")
    String first_name;
    @JsonProperty("last_name")
    String last_name;
    @JsonProperty("email")
    String email;
    @JsonProperty("gender")
    String gender;
    @JsonProperty("ip_address")
    String ip_address;
}
