package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Post {
    @JsonProperty("id")
    String id;
    @JsonProperty("slug")
    String slug;
    @JsonProperty("title")
    String title;
}
