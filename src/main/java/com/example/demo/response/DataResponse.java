package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DataResponse {
    @JsonProperty("code")
    String code;

    @JsonProperty("data")
    List<Post> data;
}
